機能一覧
===

- ユーザ登録機能
  - グループ機能
- 書籍登録機能
- 貸借管理機能
- 検索機能

## ユーザ登録機能

- ユーザ登録画面がある
  - ユーザ名
  - メールアドレス
  - パスワード
- 初回時、メールで認証確認
- OAuth
  - twitter
  - facebook
  - google
- ユーザは書籍情報を保持できる
  - ユーザが持っている書籍

- グループ機能がある
  - 企業・部署・チーム
  - グループは書籍情報を保持できる
    - グループが持っている書籍

## 書籍登録機能
- 書籍登録画面がある
  - ISBMで登録が楽？
  - Amazonリンク貼り付けると自動で書籍情報入れてくれるの楽そう
- 必要なデータ
  - 書籍名
  - ジャンル
  - 所有者

## 貸借管理機能
- ユーザ・グループ間で諸正規の貸借ができる
  - 貸借者
  - 貸借ステータス
  - 貸借日
  - 返却許定日

## 検索機能
- 以下の項目で検索したい
  - 書籍名
  - ISBM
  - ジャンル
  - 所有者

### その他あったら良さそうな機能
- 返却予定日になるとメールが届く
- 予約機能
  - 借用中の本を読みたい場合に
- 読んだ本を保持しておく機能
  - 評価できる
  - コメントできる
- 他の人が何を読んだか見れる機能
